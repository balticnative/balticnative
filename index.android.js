/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import SecondComponent from './components/native-component/SecondComponent';
import LoginView from './components/login-component/LoginView';
import RegisterView from './components/register-component/RegisterView';
import Button from 'react-native-button';


export default class BalticNative extends Component {
  constructor(props){
    super(props);
    this.handleRedirect = this.handleRedirect.bind(this);
  }

  componentWillMount(){

  }

  componentDidMount(){

  }

  handleRedirect(){
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Title
        </Text>

        <Text style={styles.instructions}>
          Components are imported below,{'\n'}
          Check styles below to implement them in the future
        </Text>

        <Button
         style={{fontSize: 20, color: 'white', backgroundColor: 'orange'}}
         styleDisabled={{color: 'red'}}
         onPress={this.handleRedirect}>
         TestButton
       </Button>

       <View>

        <RegisterView/>

       </View>


      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('BalticNative', () => BalticNative);
