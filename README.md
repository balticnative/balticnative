IMPORTANT: Including new node modules to React Native project can be quite tricky:
1. When installing the new node module using npm I suggest use - - save. Like this: "npm install <module_name> —save”
2. When the module is installed it’s still not visible to the project. And if you try to import it the app will crash. So use command ”npm install” in order to make it ”visible” for the application.
3. Close the React-packager terminal window and re-start it with good old "react-native run-android”
4. That should do it :slightly_smiling_face:

Gradle problem: cd android/ && ./gradlew clean && cd .. && react-native run-android https://github.com/airbnb/react-native-maps/issues/378

### Modules ###
npm install react-native-button --save


















# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact